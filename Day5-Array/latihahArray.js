var hobby = ["coding", "climbing", "gaming"];

console.log(hobby);
console.log(hobby.length);

console.log(hobby[0])
console.log(hobby[1])
console.log(hobby[2])

console.log(hobby[hobby.length -1])

console.log("--------------------------------------------------------------------------");


var feeling = ["dag", "dig"];
feeling.push ("dug")
console.log(feeling)

feeling.pop("dug")
console.log(feeling)

console.log("--------------------------------------------------------------------------");


var angka = [1,2,3,4,5]
angka.unshift(5)
console.log(angka)

angka.pop(6)
console.log(angka)

console.log("--------------------------------------------------------------------------");

console.log("Contoh Short")
var namaAnak = ["Hilmy","Ahmad", "Firdaus"];
namaAnak.sort() //tidak bisa digunakan short untuk angka
console.log(namaAnak)

console.log("--------------------------------------------------------------------------");

console.log("ASC");
var angka = [12,1,35,43,25]
angka.sort(function (value1, value2) {
    return value1 -value2
})
console.log(angka)

console.log("--------------------------------------------------------------------------");

console.log("DESC");
var angka = [12,1,35,43,25]
angka.sort(function (value1, value2) {
    return value2 -value1
})
console.log(angka)

console.log("--------------------------------------------------------------------------");

// console.log("Slice");
// var angka1 = [0,1,2,3,4]
// var irisan = angka.slice
// angka.sort(function (value1, value2) {
//     return value2 -value1
// })
// console.log(angka)

var data ="name: hilmy, ahad"
var name = data.split(":")
console.log(name)

console.log("-----JOIN----")
var minuman = ["saya", "suka", "jahe"]
var temp = minuman.join(" ")
console.log(temp)

console.log("Array Multidimensi")

var ArrayMulti = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
]

console.log(ArrayMulti[0][0]);
console.log(ArrayMulti[1][2]);
console.log(ArrayMulti[2][1]);
